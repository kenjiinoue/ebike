#include <WatchDog.h>
#include <SPI.h>
#include <SD.h>
#include <TimerThree.h>


//Analog Inputs
#define THROTTLE_IN  A0

//PWM
#define TLIGHT_PWM_R  2       //turn signal R
#define TLIGHT_PWM_L  3       //turn signal L
#define HLIGHT_PWM    4       //headlight

//Serial

//inputs
#define HLIGHT_IN    22      //headlight switch
#define TLIGHT_IN_R  23      //turn signal in right
#define TLIGHT_IN_L  24      //turn signal in left

//Debugging dip switches
#define DIP_0        30      //debugging dip switches
#define DIP_1        31
#define DIP_2        32
#define DIP_3        33

//Constants
#define HLIGHT_INTENSITY   255 //desired brightness for headlight
#define MAX_THROTTLE       128
#define MIN_THROTTLE       0


//Variables
int throttlepos;    //adc input for throttle position
int hlightstatus;  //0 = off, 1 = on
int tLightR;      //0 = off, 1 = right, 2 = left
int tLightL;

File fp;


void setup() {
  // put your setup code here, to run once:
  pinMode(TLIGHT_PWM_R, OUTPUT);          //right turn blinker
  pinMode(TLIGHT_PWM_L, OUTPUT);          //left turn blinker
  pinMode(HLIGHT_PWM, OUTPUT);            //headlamp

  pinMode(TLIGHT_IN_R, INPUT);            //right turn blinker
  pinMode(TLIGHT_IN_L, INPUT);            //left turn blinker
  pinMode(HLIGHT_IN, INPUT);              //headlamp

  pinMode(THROTTLE_IN, INPUT);

  pinMode(DIP_0, INPUT);
  pinMode(DIP_1, INPUT);
  pinMode(DIP_2, INPUT);
  pinMode(DIP_3, INPUT);

  
}

void loop() {
  // put your main code here, to run repeatedly:
  //read pins
  throttlepos = analogRead(THROTTLE_IN);
  tLightR = digitalRead(TLIGHT_IN_R);
  tLightL = digitalRead(TLIGHT_IN_L);
  hlightstatus = digitalRead(HLIGHT_IN);
  
  
}
