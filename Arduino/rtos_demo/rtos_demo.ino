#include <Arduino_FreeRTOS.h>
#include <semphr.h>
#include <queue.h>
#include <SPI.h>
#include <SD.h>
#include <Wire.h>


//Constants
#define TURN_SIG_INTENSITY      255
#define TURN_SIG_SPEED_MS       500
#define HIGH_BEAM_INTENSITY       255 //desired brightness for headlight
#define LOW_BEAM_INTENSITY       128 //desired PWM brightness for rear running light
#define BLIGHT_INTENSITY       255 //desired PWM brightness for brake light
#define BLIGHT_LOW             128
#define MAX_THROTTLE           128
#define MIN_THROTTLE             0

//queues
#define QUEUE_LEN               15
#define THROTTLE_POLLING_TICKS   3 //1 tick = 15ms
#define I2C_SEND_TICKS           1
#define QUEUE_TIMEOUT           10

//PIN ASSIGNMENTS
//PWM
#define RIGHT_TURN_SIG_PWM       10       //turn signal R
#define LEFT_TURN_SIG_PWM        11       //turn signal L
#define HEAD_LIGHT_PWM           13       //headlight
#define BRAKE_LIGHT_PWM          12       //brake light

//ADC
#define THROTTLE_PIN            A0

#define BATT_V_EN                7

//Serial

//inputs
#define R_BRAKE_SW              26      //right brake switch
#define L_BRAKE_SW              24      //left brake switch
#define LOW_BEAM_SIG            43      //Low Beams input
#define HIGH_BEAM_SIG           45      //High beams input
#define RIGHT_TURN_SIG             47      //turn signal in right
#define LEFT_TURN_SIG             49      //turn signal in left

#define BATT_V_SIG              A2
#define TEMP_SIG                A5

//Debugging dip switches
#define DIP_0                   30      //debugging dip switches
#define DIP_1                   31
#define DIP_2                   32
#define DIP_3                   33




typedef struct {
  byte b1 : 1;
  byte b2 : 1;
  byte b3 : 1;
  byte b4 : 1;
  byte b5 : 1;
  byte b6 : 1;
  byte b7 : 1;
  byte b8 : 1;
} i2c_rcv_bits;

typedef union {
  byte packed;
  i2c_rcv_bits bits;
} i2c_rcv_packet;

typedef union {
  byte packed;
  i2c_rcv_bits bits;
} i2c_send_packet;

typedef struct {
  byte b1 : 1;
  byte b2 : 1;
  byte b3 : 1;
  byte b4 : 1;
  byte b5 : 1;
  byte b6 : 1;
  byte b7 : 1;
  byte b8 : 1;
} bt_bits;

typedef union {
  byte packed;
  bt_bits bits;
} bt_packet;

//define two tasks for Blink & AnalogRead
void TaskTurnSignal( void *pvParameters );
void TaskBrakeLight( void *pvParameters );
void TaskThrottleRead( void *pvParameters );
void TaskSendI2cPackets( void *pvParameters);
void TaskRcvI2cPackets( void *pvParameters);
void TaskWriteSDCard( void *pvParameters);
void TaskSendBTPackets( void *pvParameters);
void TaskRcvBTPackets( void *pvParameters);

QueueHandle_t bt_send_queue, bt_rcv_queue, i2c_rcv_queue, i2c_send_queue;
SemaphoreHandle_t bt_sem, i2c_send_sem, sd_sem;

unsigned long strt, en, delta;

// the setup function runs once when you press reset or power the board
void setup() {
  strt = micros();
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  Serial1.begin(9600);
  Serial2.begin(38400);
  Serial3.begin(9600);

  //initialize pins

  //lights
  pinMode(RIGHT_TURN_SIG_PWM, OUTPUT);
  pinMode(LEFT_TURN_SIG_PWM, OUTPUT);
  pinMode(HEAD_LIGHT_PWM, OUTPUT);
  pinMode(BRAKE_LIGHT_PWM, OUTPUT);

  pinMode(LOW_BEAM_SIG, INPUT);
  pinMode(HIGH_BEAM_SIG, INPUT);
  pinMode(R_BRAKE_SW, INPUT);
  pinMode(L_BRAKE_SW, INPUT);
  pinMode(RIGHT_TURN_SIG, INPUT);
  pinMode(LEFT_TURN_SIG, INPUT);
  
  //throttle
  //no need to init analog pins

  //battery
  pinMode(BATT_V_EN, OUTPUT);
  digitalWrite(BATT_V_EN, LOW); //DISABLE BATTERY VOLTAGE ENABLE

  //Initialie i2c bus
  Wire.begin();
  
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB, on LEONARDO, MICRO, YUN, and other 32u4 based boards.
  }
  Serial.print("Serial 0 Initialized\n");
  
  while (!Serial1) {
    ; // wait for serial port to connect. Needed for native USB, on LEONARDO, MICRO, YUN, and other 32u4 based boards.
  }
  Serial.print("Serial 1 Initialized\n");

  while (!Serial2) {
    ; // wait for serial port to connect. Needed for native USB, on LEONARDO, MICRO, YUN, and other 32u4 based boards.
  }
  Serial.print("Serial 2 Initialized\n");

  while (!Serial3) {
    ; // wait for serial port to connect. Needed for native USB, on LEONARDO, MICRO, YUN, and other 32u4 based boards.
  }
  Serial.print("Serial 3 Initialized\n");

  bt_send_queue = xQueueCreate (QUEUE_LEN, sizeof(bt_packet));
  bt_rcv_queue = xQueueCreate (QUEUE_LEN, sizeof(bt_packet));
  i2c_rcv_queue = xQueueCreate (QUEUE_LEN, sizeof(i2c_rcv_packet));
  //i2c_send_queue = xQueueCreate (QUEUE_LEN, sizeof(i2c_send_packet));
  bt_sem = xSemaphoreCreateBinary();
  i2c_send_sem = xSemaphoreCreateBinary();
  sd_sem = xSemaphoreCreateBinary();

  if (!bt_send_queue) {
    Serial.print("Bluetooth queue failed to allocate like wtf\n");    
  }
  if (!i2c_rcv_queue) {
    Serial.print("i2c receive queue failed to allocate like wtf\n");    
  }
  if (!i2c_send_queue) {
    Serial.print("i2c send queue failed to allocate like wtf\n");    
  }
  if (!bt_sem) {
    Serial.print("bt send sem failed to allocate like wtf\n");    
  }
  if (!i2c_send_sem) {
    Serial.print("i2c send semaphore failed to allocate like wtf\n");    
  }

  //Create Tasks
  xTaskCreate(
    TaskTurnSignal
    ,  (const portCHAR *)"Turn Signals"   // A name just for humans
    ,  128  // This stack size can be checked & adjusted by reading the Stack Highwater
    ,  NULL
    ,  2  // Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
    ,  NULL );
    
  xTaskCreate(
    TaskBrakeLight
    ,  (const portCHAR *)"Turn Signals"   // A name just for humans
    ,  128  // This stack size can be checked & adjusted by reading the Stack Highwater
    ,  NULL
    ,  1  // Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
    ,  NULL );
//  xTaskCreate(
//    TaskSendI2cPackets
//    ,  (const portCHAR *) "i2c send"
//    ,  128  // Stack size
//    ,  NULL
//    ,  1  // Priority
//    ,  NULL );  
//
//  xTaskCreate(
//    TaskRcvI2cPackets
//    ,  (const portCHAR *) "i2c rcv"
//    ,  128  // Stack size
//    ,  NULL
//    ,  1  // Priority
//    ,  NULL );
//    
//  xTaskCreate(
//    TaskWriteSDCard
//    ,  (const portCHAR *) "WriteSDCard"
//    ,  128  // Stack size
//    ,  NULL
//    ,  1  // Priority
//    ,  NULL );
//    
  xTaskCreate(
    TaskSendBTPackets
    ,  (const portCHAR *) "Send BT Packets"
    ,  128  // Stack size
    ,  NULL
    ,  0  // Priority
    ,  NULL );

//  xTaskCreate(
//    TaskRcvBTPackets
//    ,  (const portCHAR *) "Rcv BT Packets"
//    ,  128  // Stack size
//    ,  NULL
//    ,  1  // Priority
//    ,  NULL );
//
//    
//  xTaskCreate(
//    TaskThrottleRead
//    ,  (const portCHAR *) "ThrottleRead"
//    ,  128  // Stack size
//    ,  NULL
//    ,  1  // Priority
//    ,  NULL );

  //Print out setup run time
  en = micros();
  delta = en - strt;
  Serial.println("Setup Time: " + (String) delta);
}

void loop()
{
  // Empty. Things are done in Tasks.
}

/*--------------------------------------------------*/
/*---------------------- Tasks ---------------------*/
/*--------------------------------------------------*/
void TaskSendI2cPackets( void *pvParameters){
  (void) pvParameters;
  i2c_send_packet *pkt;
  for(;;){
    if( xQueueReceive( i2c_send_queue, &( pkt ), ( TickType_t ) QUEUE_TIMEOUT ) == pdFALSE )
        {
            Serial.print("TaskSendI2cPackets: Queue Rcv Fail\n");
            break;
        }
    Serial.print("TaskSendI2cPackets: Throttle Position = " + (String) pkt->packed);
    //send over i2c here. Pause for a bit

    vTaskDelay(I2C_SEND_TICKS); //maybe just send this asap with no delay? who tf knows
  }
}


//this task will receive rpm data from the inverter and then dispatch it to the bluetooth queue
void TaskRcvI2cPackets( void *pvParameters){
  (void) pvParameters;
  for(;;){
    
  }
}

/*********************************************************************************************
 * 
 *********************************************************************************************/
void TaskWriteSDCard( void *pvParameters){
  (void) pvParameters;
  for(;;){}
}

/*********************************************************************************************
 * 
 ********************************************************************************************/
void TaskSendBTPackets( void *pvParameters){
  (void) pvParameters;
  for(;;){
      Serial2.println("69");
      Serial.print("sent 69\n");
      vTaskDelay(100);
    }
}


/*********************************************************************************************
 * 
 ********************************************************************************************/
void TaskRcvBTPackets( void *pvParameters){
  (void) pvParameters;

  for(;;){}
  
}
/*********************************************************************************************
 * I'll make this shit interrupt-driven at some point but didn't have time to look it up and
 * implement it
 ********************************************************************************************/
void TaskBrakeLight(void *pvParameters){
  (void) pvParameters;
  int intensity, l,r;

  for(;;){
     r = digitalRead(R_BRAKE_SW);
     l = digitalRead(L_BRAKE_SW);

     intensity =  (r || l) ? BLIGHT_INTENSITY:BLIGHT_LOW;

     analogWrite(BRAKE_LIGHT_PWM, intensity);
     vTaskDelay(100 / portTICK_PERIOD_MS);
  }
}
 /*********************************************************************************************
 * TaskTurnSignal
 * This function is responsible for the turn signal. Usually it is stuck inside the while loop.
 * It polls every 100ms for a turn signal trigger. Maybe this can be done more efficiently.
 * Explore later lmao im too lazy
 * Also blinks built in LED pin
 * 
 *********************************************************************************************/
void TaskTurnSignal(void *pvParameters)  // This is a task.
{
  (void) pvParameters;
  int r, l, hb, lb, flight_lvl;

  for (;;) // A Task shall never return or exit.
  {
    //read inputs
    r = digitalRead(RIGHT_TURN_SIG);
    l = digitalRead(LEFT_TURN_SIG);
    hb = digitalRead(HIGH_BEAM_SIG);
    lb = digitalRead(LOW_BEAM_SIG);

    //front light logic
    if      (hb)  flight_lvl = HIGH_BEAM_INTENSITY;
    else if (lb)  flight_lvl = LOW_BEAM_INTENSITY;
    else          flight_lvl = 0;
    //set front light level
    analogWrite(HEAD_LIGHT_PWM, flight_lvl);

    //turn signal
    if (r){
      analogWrite(RIGHT_TURN_SIG_PWM, TURN_SIG_INTENSITY);
      vTaskDelay( TURN_SIG_SPEED_MS / portTICK_PERIOD_MS ); // wait for .5s
      analogWrite(RIGHT_TURN_SIG_PWM, 0);
      vTaskDelay( TURN_SIG_SPEED_MS / portTICK_PERIOD_MS ); // wait for .5s
    }
    else if (l){
      analogWrite(LEFT_TURN_SIG_PWM, TURN_SIG_INTENSITY);
      digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
      vTaskDelay( TURN_SIG_SPEED_MS / portTICK_PERIOD_MS ); // wait for .5s
      analogWrite(LEFT_TURN_SIG_PWM, 0);
      digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
      vTaskDelay( TURN_SIG_SPEED_MS / portTICK_PERIOD_MS ); // wait for .5s
    }
    else {
      vTaskDelay(100 / portTICK_PERIOD_MS);
    }
  }
}


 /*********************************************************************************************
 * TaskThrottleRead
 * This function polls an adc pin for throttle position data every 45ms and sends it to the 
 * inverter.
 * 
 * TODO: Add more safety features for stuck throttle?
 * 
 *********************************************************************************************/
void TaskThrottleRead(void *pvParameters) {
  (void) pvParameters;
  
  int throttleVal;
  i2c_send_packet pkt;

  for (;;) {
    // read the input on analog pin 0:
    throttleVal = analogRead(THROTTLE_PIN);
    if (throttleVal > MAX_THROTTLE) {
      throttleVal = MAX_THROTTLE;
      Serial.print("TaskThrottleRead: Throttle limited by MAX_THROTTLE\n");
    }
    
    // print out the value you read:
    Serial.print("throttleVal: " + (String) throttleVal + "\n");

    //copy value to packet structure
    pkt.packed = (byte) throttleVal;
    
    //dispatch to queue
    if( xQueueSendToBack( i2c_send_queue, ( void * ) &pkt, ( TickType_t ) QUEUE_TIMEOUT ) != pdPASS ) {
      Serial.print("TaskThrottleRead: Fail - Queue Full and timed out\n");
      break;
    } 
    //Delay. 1 tick = 15ms
    vTaskDelay(THROTTLE_POLLING_TICKS);  
  }
  Serial.print("TaskThrottleRead: Task Exited.\n");
}